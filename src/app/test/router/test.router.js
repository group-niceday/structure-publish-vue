const TestRouter = [
  {
    path:      '/test',
    component: () => import ('@/app/system/layout/default.vue'),
    redirect:  '/test/test1',
    children:  [
      {
        path:      'test1',
        component: () => import('@/app/test/view/test1.vue'),
      },
      {
        path:      'test2',
        component: () => import('@/app/test/view/test2.vue'),
      }
    ]
  }
];

export default TestRouter;
