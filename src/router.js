import Router from 'vue-router';

const requireContext = require.context('@/', true, /\w+.router.js$/);
const routeConfigs = [];
requireContext.keys().forEach(fileName => [...requireContext(fileName).default].forEach(route => routeConfigs.push(route)));

const routerMethods = ['push', 'replace'];

routerMethods.forEach((method) => {
  const originalCall = Router.prototype[method];
  Router.prototype[method] = function (location, onResolve, onReject) {
    if (onResolve || onReject) {
      return originalCall.call(this, location, onResolve, onReject);
    }

    return originalCall.call(this, location).catch((err) => err);
  };
});

const router = new Router({
  mode:   'history',
  routes: routeConfigs
});

export default router;
