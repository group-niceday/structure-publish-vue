import Vue       from 'vue'
import VueRouter from 'vue-router';

import App    from '@/app.vue'
import router from '@/router'

import '@/assets/sass/style.scss';

Vue.config.devtools = process.env.NODE_ENV === 'local';
Vue.config.performance = process.env.NODE_ENV === 'local';
Vue.config.silent = process.env.NODE_ENV === 'production';
Vue.config.productionTip = false;

Vue.use(VueRouter);


new Vue({
  router,
  render: h => h(App)
}).$mount('#app')
