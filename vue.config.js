const webpack = require('webpack');
const ansiRegex = require('ansi-regex');
const CompressionPlugin = require('compression-webpack-plugin');
// const BundleAnalyzerPlugin = require('webpack-bundle-analyzer')
//   .BundleAnalyzerPlugin;

module.exports = {
  configureWebpack: (config) => {
    config.entry = ["babel-polyfill", "./src/main.js"];
    if (process.env.VUE_CLI_MODERN_MODE) {
      config.output.filename = '[name].[hash:16].js';

      // config.plugins.push(new BundleAnalyzerPlugin({
      //   analyzerHost: 'local.genie.co.kr',
      //   analyzerPort: 8889,
      //   openAnalyzer: true,
      //   analyzerMode: 'static'
      // }));

      config.plugins.push(new webpack.IgnorePlugin(/^\.\/locale$/, /moment$/));
    }
  },
  chainWebpack: config => {
    if (process.env.VUE_CLI_MODERN_MODE) {
      config.optimization.minimize(true)
    }
    config.plugins.delete('prefetch');
    config.plugin('CompressionPlugin').use(CompressionPlugin);
  },
  transpileDependencies: [ansiRegex],
  devServer: {
    historyApiFallback: true,
    compress: true,
    publicPath: '/',
    host: "0.0.0.0",
    port: 8888,
    public: "local.genie.co.kr:8888"
  }
}
